import h5py
from h5py import Group, Dataset
import numpy as np

with h5py.File("BVCA_CX3322A Data Log.hdf5", mode='r') as f:
    if f.name == "/":
        if type(f.parent) is Group:
            final_data = {}
            bv_Dataset__Variables = f.get('__BV_Dataset__Variables__')
            bv__Dataset_Attributes = f.get('__BV__Dataset_Attributes__')
            bv_Dataset__Data = f.get("__BV_Dataset__Data__")
            if type(bv_Dataset__Data.parent) is Group:
                Dataset__Data = {}
                for i in bv_Dataset__Data:
                    a = bv_Dataset__Data.get(i)
                    if i == "Time":
                        for b in a:
                            Dataset__Data[b] = np.array(a.get(b))
                            print(a.get(b).name)
                    else:
                        Dataset__Data[i] = np.array(a.get(i))
                final_data["__BV_Dataset__Data__"] = Dataset__Data
            if type(bv__Dataset_Attributes.parent) is Group:
                dataset_Attributes ={}
                for i in bv__Dataset_Attributes:
                    if type(bv__Dataset_Attributes.get(i)) is Dataset:
                       dataset_Attributes[i] = np.array(bv__Dataset_Attributes.get(i))
                final_data["__BV__Dataset_Attributes__"] = dataset_Attributes
            if type(bv_Dataset__Variables.parent) is Group:
                Dataset__Variables = {}
                for i in bv_Dataset__Variables:
                    a = bv_Dataset__Variables.get(i)
                    inner_data = {}
                    if type(a) is Group:
                        for innerAtter in a:
                            if type(a.get(innerAtter)) is Dataset:
                                inner_data[innerAtter] = np.array(a.get(innerAtter))
                    Dataset__Variables[i] = inner_data
                final_data['__BV_Dataset__Variables__'] = Dataset__Variables
        print(final_data)
    f.close()
